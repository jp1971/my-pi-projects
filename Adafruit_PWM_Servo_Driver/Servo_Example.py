#!/usr/bin/python

from Adafruit_PWM_Servo_Driver import PWM
import time
import socket

UDP_IP = "192.168.1.8"
UDP_PORT = 5005

# ===========================================================================
# Example Code
# ===========================================================================

# Initialise the PWM device using the default address
# bmp = PWM(0x40, debug=True)
pwm = PWM(0x40, debug=True)

servoMin = 150  # Min pulse length out of 4096
servoMax = 500  # Max pulse length out of 4096

def setServoPulse(channel, pulse):
  pulseLength = 1000000                   # 1,000,000 us per second
  pulseLength /= 60                       # 60 Hz
  print "%d us per period" % pulseLength
  pulseLength /= 4096                     # 12 bits of resolution
  print "%d us per bit" % pulseLength
  pulse *= 1000
  pulse /= pulseLength
  pwm.setPWM(channel, 0, pulse)

pwm.setPWMFreq(60)                        # Set frequency to 60 Hz

sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))

while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print "received message:", data
    
    if data=="J":
        pwm.setPWM(0, 0, 175)
    elif data=="K":
        pwm.setPWM(0, 0, 425)
    elif data=="L":
        pwm.setPWM(0, 0, 675)
    elif data=="I":
        pwm.setPWM(1, 0, 675)
    elif data=="M":
        pwm.setPWM(1, 0, 425)
"""        
while (True):
  # Change speed of continuous servo on channel O
  pwm.setPWM(0, 0, servoMin)
  time.sleep(1)
  pwm.setPWM(0, 0, servoMax)
  time.sleep(1)
"""


